<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// /*Route::get('/', function () {
//     return view('aboutus');
// });*/
// Route::get('/', 'indexController@index');
// Route::get('/index', 'indexController@index');
// Route::get('benefits', 'benefitsController@index');
// Route::get('aboutus', 'aboutusController@index');
// Route::get('contactus', 'contactusController@index');

// Route::get('/index{id}/edit', 'indexController@edit');
// Route::get('/index{id}/update', 'indexController@update');

// Route::get('/benefits{id}/edit', 'benefitsController@edit');
// Route::get('/benefits{id}/update', 'benefitsController@update');

// Route::get('/aboutus{id}/edit', 'aboutusController@edit');
// Route::get('/aboutus{id}/update', 'aboutusController@update');

// Route::get('/contactus{id}/edit', 'contactusController@edit');
// Route::get('/contactus{id}/update', 'contactusController@update');

// Route::get('fitur-aplikasi-reprime-attendance-tracking-reporting', 'WebFrontController@getFitur');
// Route::get('demo-aplikasi-reprime', 'WebFrontController@getDemo');
// Route::get('contact-aplikasi-reprime', 'WebFrontController@getContact');
// Route::get('about-aplikasi-reprime', 'WebFrontController@getAbout');
// Route::get('absense-aplikasi-reprime', 'WebFrontController@getAbsense');
// Route::get('privacy-policy', 'WebFrontController@getPrivacy');
// Route::get('term-condition', 'WebFrontController@getTerm');
// Route::get('blogs', 'WebFrontController@getBlogs');
// // Route::get('blog/{id}', 'WebFrontController@getContentBlog');

// Route::get('blog/{id}/{name}', 'WebFrontController@getContentBlogAbsen');
// Route::get('previous/{id}/{name}', 'WebFrontController@getContentPrevBlogAbsen');
// Route::get('next/{id}/{name}', 'WebFrontController@getContentNextBlogAbsen');

// Route::get('view/{id}', 'WebFrontController@getContentCase');
// Route::get('wise', 'WebFrontController@getWise');
// Route::get('suc', 'WebFrontController@getSuc');



// Route::post('send-mail','SendMailController@sendMail');

// Route::get('tabel-view-fitur-absen','BackEndController@getViewFitur');
// Route::post('Add/Fitur','BackEndController@getAddFitur');
// Route::delete('Delete/Fitur','BackEndController@getDeleteFitur');


/* MPM */ 
Route::get('/', function ()
{
    return view('web.home.home');
});
/* END OF MPM */ 