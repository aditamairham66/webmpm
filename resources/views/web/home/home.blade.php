@extends('web.template.index')
@section('content')
<div class="baner-home">
    <div class="container">
    <div class="konten-baner-home">
        <div class="text-baner">
            A Wide Range Of <b>Two-Wheel And Four-Wheel</b> Aftermarket Oil And Spare Parts <br> <b>Distribution Services.</b>
        </div>    
    </div>
    </div>
    <img src="{{asset('mpm/img/bg_header.png')}}" alt="">
</div>
<br><br>

<div class="container">
    <div class="product">
        <div class="judul">
            <h3><b>OUR</b> PRODUCT.</h3>
            <span>THE BEST OF THE BEST, ALWAYS</span>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="product">
        <div class="judul">
            <h3><b>INTRODUCING</b></h3>
            <span>PHOTO OF OUR ACTIVITIES</span>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="product">
        <div class="judul">
            <h3><b>OUR ACTIVITIES</b></h3>
            <span>PHOTO OF OUR ACTIVITIES</span>
        </div>
    </div>
</div>
<script>

</script>
<style>
    /* BANER HOME */
    .baner-home{
        width:100%;height:657px;
    }
    .baner-home .konten-baner-home{
        position:absolute;margin-top:260px
    }
    .baner-home .konten-baner-home .text-baner{
        color:#fff;font-size:39px;width:80%;letter-spacing:2px;line-height: 150%;transform:scale(1,1.1);
    }
    .baner-home img{
        width:100%;height:100%;
    }
    /* END BANER HOME */
    /* PRODUCT */
    .product{
        margin-top:80px;
    }
    .product .judul{
        margin-bottom:50px;text-align:center;
    }
    .product .judul h3{
        font-size:30px;transform:scale(1,1.2);color:#000;
    }
    .product .judul span{
        font-size:14px;color:#A5A5A5;
    }
    /* END PRODUCT */
    @media only screen and (max-width:768px) {
        .baner-home{
            height:557px;
        }
        .baner-home .konten-baner-home{
            margin-top:240px
        }
        .baner-home .konten-baner-home .text-baner{
            font-size:25px;line-height: 170%;margin:auto 50px;width:90%;
        }
    }
    @media only screen and (max-width:564px) {
        .baner-home{
            height:457px;
        }
        .baner-home .konten-baner-home{
            margin-top:180px;
        }
        .baner-home .konten-baner-home .text-baner{
            font-size:20px;line-height: 150%;margin:auto 40px;width:85%;
        }
    }

</style>
@endsection