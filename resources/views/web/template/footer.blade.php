<div class="baner-footer">
    <div class="container"> 
        <div class="baner-footer-koten" style="position:absolute;">        
            <div class="img-footer">
                <img src="{{ asset('mpm/img/logo_mpm.png')}}" alt="">
            </div>
            <div class="sosial-footer">
                <ul class="icon-sosial">
                    <li><a href=""><i class="fab fa-instagram"></i></a></li>
                    <li><a href=""><i class="fab fa-facebook-square"></i></a></li>
                </ul>
            </div>
            <ul class="address-footer">
                <li class="header">
                    Jl. jalur sutera Kav.A9 Alam Sutera, Kunciran, Tangerang, Kota Tangerang, Banten 15415
                    Office Hour : 8AM - 5PM
                </li>
                <li><i class="fa fa-phone"></i> &nbsp;(021) 30429999</li>
                <li><i class="fa fa-envelope"></i> &nbsp;customerservice@mpm-parts.com</li>
            </ul>
            <ul class="menu-footer kiri-footer">
                <li class="header">
                    Home
                </li>
                <li><a href="">Upcoming Events</a></li>
                <li><a href="">Promotions</a></li>
            </ul>
            <ul class="menu-footer">
                <li class="header">
                    Product
                </li>
                <li><a href="">Daytona</a></li>
                <li><a href="">Federal Oil</a></li>
                <li><a href="">Federal Mobil</a></li>
            </ul>
            <ul class="menu-footer">
                <li class="header">
                    News & Media About
                </li>
                <li><a href="">Visi & Misi</a></li>
                <li><a href="">Our Network</a></li>
            </ul>
            <ul class="menu-footer">
                <li class="header">
                    Distributional Channel
                </li>
                <li><a href="">Distributional Channel</a></li>
            </ul>
        </div>
    </div>

    <img src="{{asset('mpm/img/bg_footer_0.png')}}" alt="" style="width:100%;height:100%;">
</div>
<div class="copyright">
    Copyright 2019 MPM Parts
</div>

<script src="{{ asset('js/app.js') }}"></script>
<style>
    .baner-footer{
        width:100%;height:410px;
    }
    .img-footer{
        margin-top:40px;
        width:120px;
        height:80px;
    }
    .img-footer img{
        width:100%;
        height:100%;
    }
    .sosial-footer{
        margin-top:-60px;
        position: relative;
        margin-left:1030px;
    }
    .sosial-footer .icon-sosial li{
        float:left;
        list-style:none;
        margin-right:22px;
    }
    .sosial-footer .icon-sosial li a{
        color:#fff;
        font-size:28px;
    }
    .address-footer{
        margin:80px -40px;
        width:390px;
        color:#fff;
        float:left;
    }
    .address-footer li{
        list-style:none;
        margin-bottom:10px;
        font-size:16px;
    }
    .address-footer li .fa{
        color:#f26f21;
    }

    .menu-footer{
        float:left;
        margin-top:80px;
        margin-right:20px;
        width:190px;
    }
    .menu-footer li{
        list-style:none;
        font-size:16px;
        margin-bottom:6px;
        color:#fff;
    }
    .menu-footer li a{
        color:#fff;
    }
    .menu-footer .header{
        list-style:none;
        font-size:16px;
        margin-bottom:10px;
        color:#f26f21;
        font-weight:700;
    }
    .kiri-footer{
        margin-left:60px;
    }
    /* copyright */
    .copyright{
        width:100%;margin:17px auto;text-align:center;font-size:14px;color:#000;
    }
    @media only screen and (max-width:768px) {
        .sosial-footer{
            margin-top:-60px;
            margin-left:530px;
        }
        .address-footer{
            width:700px;
        }
        .menu-footer{
            margin-top:-20px;
            margin-right:10px;
            width:170px;
        }
        .kiri-footer{
            margin-left:-40px;
        }
        .baner-footer{
            height:480px;
        }
    }
    @media only screen and (max-width:564px) {
        .sosial-footer{
            margin-top:-60px;
            margin-left:200px;
        }
        .address-footer{
            width:400px;
        }
        .menu-footer{
            margin-top:-20px;
            margin-bottom:40px;
            width:180px;
            margin-left:-10px;
        }
        .kiri-footer{
            margin-left:-10px;
        }
        .baner-footer{
            height:670px;
        }
    }

    @media only screen and (max-width:360px) {
        .sosial-footer{
            margin-top:-60px;
            margin-left:200px;
        }
        .address-footer{
            width:350px;
        }
        .menu-footer{
            margin-top:-20px;
            margin-bottom:40px;
            width:200px;
            margin-left:-10px;
        }
        .kiri-footer{
            margin-left:-10px;
        }
        .baner-footer{
            height:970px;
        }
    }
</style>
</body>
</html>