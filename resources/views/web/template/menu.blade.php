<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
          <img src="{{ asset('mpm/img/logo_mpm.png')}}" alt="" class="logo-mpm">
      </a>
    </div>
    <div class="navbar-right">
        <div class="menu-right">
        <span class="fa fa-list"></span>
        </div>
    </div>

    <ul class="nav navbar-nav">
      <li class="active"><a href="#">HOME</a></li>
      <li><a href="#">PROFILE</a></li>
      <li><a href="#">PRODUCTS</a></li>
      <li><a href="#">OUR NETWORKS</a></li>
      <li><a href="#">OUR PARTNERS</a></li>
      <li><a href="#">UPDATES</a></li>
      <li><a href="#">CAREER</a></li>
      <li><a href="#">CONTACT</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right satu" style="padding-left:0;margin-right:-20px;">
        <li><a href="#"> INDONESIA</a></li>
        <li class="active"><a href="#"> ENGLISH</a></li>
    </ul>
  </div>
</nav>

<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="close-btn">&times;</a>
    <ul class="bahasa">
        <li><a href="javascript:void(0)"> ENGLISH</a></li>
        <li><a href="javascript:void(0)"> INDONESIA</a></li>
    </ul>

    <ul class="menu-sidenav-link">
        <li class="aktif"><a href="#">HOME</a></li>
        <li><a href="#">PROFILE</a></li>
        <li><a href="#">PRODUCTS</a></li>
        <li><a href="#">OUR NETWORKS</a></li>
        <li><a href="#">OUR PARTNERS</a></li>
        <li><a href="#">UPDATES</a></li>
        <li><a href="#">CAREER</a></li>
        <li><a href="#">CONTACT</a></li>
    </ul>
</div>

<script>
    $(function () {
       $(".menu-right").click(function(){
            $("#mySidenav").css("width","250px");
       });
       $(".close-btn").click(function(){
            $("#mySidenav").css("width","0px");
       }); 
    });
</script>
<style>
    /* HEADER */
    .logo-mpm{
        margin:-5px 15px 0px -4px;
    }
    .satu li a{
        padding:15px 5px;
    }
    /* MENU */
    .fa-list{
        color:#000;margin:15px 50px;font-size:30px;display:none;
    }
    .menu-right .fa-list:hover{
        cursor:pointer;
    }
    /* END HEADER */
    /* SIDENAV */
    .sidenav {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 9999;
        top: 0;
        left: 0;
        background-color: #fff;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
        box-shadow:1px 1px 1px #ddd;
    }
    /* CLOSE */
    .sidenav .close-btn {
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px;
        margin-left: 50px;
        color:#d2d2d2;
    }
    /* BAHASA */
    .bahasa{
        position: absolute;
        top: 0;
        left: -20px;
        margin-right: 30px;
        margin-top:25px;
    }
    .bahasa li{
        float:left;
        margin-right:10px;
        list-style:none;
    }
    .bahasa li a{
        padding:5px 10px;
        color:#F26F21;
        font-size:7px;
        letter-spacing:1px;
        background:transparent;
        border:solid 1px #F26F21;
        border-radius:50px;
        text-decoration:none;
        font-weight:700;
    }
    .bahasa li a:hover,.bahasa li a:focus{
        color:#fff;
        background:#F26F21;
        border:solid 1px #fff;
    }
    /* MENU SIDENAV */
    .menu-sidenav-link{
        margin-top:20px;
        margin-left:-20px;
    }
    .menu-sidenav-link li{
        list-style:none;
        margin:auto 0;
        margin-bottom:12px;
    }
    .menu-sidenav-link li a{
        color:#000;
        font-weight:700;
        font-size:13px;
        text-decoration:none;
    }
    .menu-sidenav-link.aktif{
        color:#F26F21;
    }
    .menu-sidenav-link li a:hover,.menu-sidenav-link li a:focus{
        color:#F26F21;
    }
    /* END SIDENAV */

    @media only screen and (max-width:768px) {
        .nav{
            display:none;
        }
        .fa-list{
            display:block;
        }
    }
    @media only screen and (max-width:564px) {
        .logo-mpm{
            width:80px;
            height:40px;
            margin:0px 0px 0px 15px;
        }
        .navbar-inverse{
            padding:10px 0;
        }
        .nav{
            display:none;
        }
        .menu-right{
            float:right;
        }
        .fa-list{
            margin-top:-40px;
            margin-bottom:0;
            margin-right:25px;
            display:block;
        }
    }

</style>